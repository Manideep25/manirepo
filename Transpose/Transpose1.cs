﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transpose
{
    class Transpose1
    {
        static void Main(string[] args)
        {
            Console.WriteLine("enter the numbere of rows and columns: ");

            int rows = Convert.ToInt32(Console.ReadLine());
            int columns = Convert.ToInt32(Console.ReadLine());

            int[,] array = new int[rows, columns];

            Console.WriteLine("enter the matrix elements: ");
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    array[i, j] = Convert.ToInt32(Console.ReadLine());
                }
            }
            Console.WriteLine("matrix is: ");
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    Console.Write(array[i, j] + " ");
                }
                Console.WriteLine();
            }
            TransposeMatrix(array, rows, columns);
        }

        private static void TransposeMatrix(int[,] array, int rows, int columns)
        {

            Console.WriteLine("transpose of the matrix is: ");
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    Console.Write(array[j, i] + " ");
                }
                Console.WriteLine();
            }
            Console.ReadLine();

        }
    }
}
